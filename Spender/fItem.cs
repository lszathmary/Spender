﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Drawing;

namespace WindowsFormsApplication1
{
    /* the item the user has brought, can be different types: Food, Drink, Medication etc
    */
    public class fItem
    {
        public enum fType {
            Food,
            Drinks,
            Fun,
            Gym,
            Car,
            Bills,
            Shopping,
            Clothing,
            Travel,
            Medication,            
            Other,   
        };

        public int id;           // each item has an unique id
        private fType type;      // type of the item 
        private int money;       // how much money does it cost
        private DateTime date;   // the date when we bought it
        private string tag;      // custom tag

        // .constructor
        public fItem()
        {
            this.type = fType.Food;
            this.money = 0;
            this.Date = DateTime.Now;
            this.tag = "";
        }

        public fType Type
        {
            set { this.type = value;  }
            get { return this.type;   }
        }

        public int Money
        {
            set { this.money = value; }
            get { return this.money;  }
        }

        public DateTime Date
        {
            set { this.date = value; }
            get { return this.date;  }
        }

        public string Tag
        {
            set { this.tag = value; }
            get { return this.tag; }
        }

        // .convert the item type to string
        public string TypeToString()
        {
            switch (type)
            {
                case fType.Food:
                    return "Food";
                case fType.Drinks:
                    return "Drinks";
                case fType.Fun:
                    return "Fun";
                case fType.Gym:
                    return "Gym";
                case fType.Car:
                    return "Car";
                case fType.Bills:
                    return "Bills";
                case fType.Shopping:
                    return "Shopping";
                case fType.Clothing:
                    return "Clothing";
                case fType.Travel:
                    return "Travel";
                case fType.Medication:
                    return "Medication";
                case fType.Other:
                    return "Other";
            }

            return "";
        }

        // we display each type with different color in the listview
        public Color TypeToColor()
        {
            switch (type)
            {
                case fType.Food:
                    return Color.Red;
                case fType.Drinks:
                    return Color.DarkOrange;
                case fType.Fun:
                    return Color.Green;
                case fType.Gym:
                    return Color.Blue;
                case fType.Car:
                    return Color.Brown;
                case fType.Bills:
                    return Color.Black;
                case fType.Shopping:
                    return Color.Indigo;
                case fType.Clothing:
                    return Color.Purple;
                case fType.Travel:
                    return Color.YellowGreen;
                case fType.Medication:
                    return Color.Olive;
                case fType.Other:
                    return Color.Turquoise;
            }

            return Color.Black;
        }

        // needed for the chart form 
        static public Color TypeToColor(fType _type)
        {
            switch (_type)
            {
                case fType.Food:
                    return Color.Red;
                case fType.Drinks:
                    return Color.DarkOrange;
                case fType.Fun:
                    return Color.Green;
                case fType.Gym:
                    return Color.Blue;
                case fType.Car:
                    return Color.Brown;
                case fType.Bills:
                    return Color.Black;
                case fType.Shopping:
                    return Color.Indigo;
                case fType.Clothing:
                    return Color.Purple;
                case fType.Travel:
                    return Color.YellowGreen;
                case fType.Medication:
                    return Color.Olive;
                case fType.Other:
                    return Color.Turquoise;
            }

            return Color.Black;
        }
    }
}
