﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class ItemEditor : Form
    {
        private fItem item = new fItem();

        // .constructor
        public ItemEditor()
        {
            InitializeComponent();

            //. so in the combobox we can select fType(Food,Fun,Gym,Other) values
            this.comboBox_Type.DataSource = Enum.GetValues(typeof(fItem.fType));
        }

        // .set or gets the item we will be editing
        public fItem Item
        {
            get
            {
                return item;
            }
            set
            {
                this.item = value;

                //. "copy" data from item to the Form 
                this.comboBox_Type.SelectedIndex = (int)item.Type;
                this.textBox_Money.Text = Convert.ToString(item.Money);
                this.textBox_Tag.Text = item.Tag;
                this.dateTimePicker1.Value = item.Date;
            }
        }

        //. the user clicked the ok button
        private void OK_Click(object sender, EventArgs e)
        {
            //. "save" the data to item variable
            this.item.Type  = (fItem.fType)comboBox_Type.SelectedIndex;
            this.item.Money = Convert.ToInt32(textBox_Money.Text);
            this.item.Tag   = textBox_Tag.Text;
            this.item.Date = dateTimePicker1.Value;

            this.DialogResult = DialogResult.OK;
        }

        //. the user clicked the cancel button
        private void Cancel_Click(object sender, EventArgs e)
        {
            //. do nothing
        }
    }
}
