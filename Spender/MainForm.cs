﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace WindowsFormsApplication1
{
    public partial class MainForm : Form
    {
        private ItemEditor   itemEditor;  // .item editor dialog box
        private ChartForm    chartForm;   // .chart dialog box
        private List<fItem>  itemList;    // . list of current items;   
        
        private int id;  // last id we "generated"

        public MainForm()
        {
            InitializeComponent();

            itemEditor = new ItemEditor();
            chartForm = new ChartForm();
            itemList= new List<fItem>();
        }

        // .the user clicked the add button so we add a new item to the itemList and the listview1
        private void Add_Click(object sender, EventArgs e)
        {
            //. init fitem variabile
            fItem item = new fItem();
                        
            // .associate wit the itemEditor
            itemEditor.Item = item;

            //. display the itemEditor dialog box
            itemEditor.ShowDialog();

            if (itemEditor.DialogResult == DialogResult.OK)
            {
                item = this.itemEditor.Item;
                item.id = ++id; // generate a new id

                itemList.Add(item); // add the item to our internal List

                //. creat the list view item
                ListViewItem listViewItem = new ListViewItem();

                //. set the text color
                listViewItem.ForeColor = item.TypeToColor();

                //. store the ide in the listviewitem`s tag
                listViewItem.Tag = id;
                
                listViewItem.Text = item.TypeToString();                            // type                     
                listViewItem.SubItems.Add(Convert.ToString(item.Money) + "$");      // money
                listViewItem.SubItems.Add(item.Tag);                                // tag
                listViewItem.SubItems.Add(item.Date.ToString("dd/MM/yyyy"));        // date
                
                // .finally add the item to the list view
                listView1.Items.Add(listViewItem);
            }

        }

        // the user clicked the edit button
        private void Edit_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 0) return; // nothing is selected

            fItem t_item = null;

            //. search for the selected item by id in the itemList
            foreach (fItem temp in this.itemList)
            {
                if (temp.id == (int)listView1.SelectedItems[0].Tag) t_item = temp;
            }

            itemEditor.Item = t_item;
            itemEditor.ShowDialog();

            // .if the user clicked ok modify the values in the listView
            if (itemEditor.DialogResult == DialogResult.OK)
            {
                listView1.SelectedItems[0].Text = t_item.TypeToString();                            // type
                listView1.SelectedItems[0].ForeColor = t_item.TypeToColor();                        // text color
                listView1.SelectedItems[0].SubItems[1].Text = Convert.ToString(t_item.Money);       // money
                listView1.SelectedItems[0].SubItems[2].Text = t_item.Tag;                           // tag
                listView1.SelectedItems[0].SubItems[3].Text = t_item.Date.ToString("dd/MM/yyyy") ;  // data
            }
        }

        // .the user clicked the delete button
        private void Delete_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 0) return; // nothing is selected

            fItem t_item = new fItem();

            //. search for the selected item by id in the itemList
            foreach (fItem temp in this.itemList)
            {
                if (temp.id == (int)listView1.SelectedItems[0].Tag) t_item = temp;
            }

            // remove the item from List<> and the listview1
            this.itemList.Remove(t_item);
            listView1.Items.Remove(listView1.SelectedItems[0]);
        }

        // the user clicked the save xml button
        private void SaveXML_Click(object sender, EventArgs e)
        {
            // .start "browsing" in the application directory
            saveFileDialog1.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory;

            // .display save dialog box
            if (saveFileDialog1.ShowDialog() == DialogResult.Cancel) return;
            
            // .write the itemList to an xml file
            using (StreamWriter stream = new StreamWriter(saveFileDialog1.FileName))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(List<fItem>));

                serializer.Serialize(stream, this.itemList);
            }
        }

        // the user cliked the load xml button
        private void LoadXML_Click(object sender, EventArgs e)
        {
            // .start "browsing" in the application directory
            openFileDialog1.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory;

            // .dipslay open dialog box
            if (openFileDialog1.ShowDialog() == DialogResult.Cancel) return;

            // .delete previous items
            this.listView1.Items.Clear();
            this.itemList.Clear();

            // .read the itemList from the xml file
            using (StreamReader reader = new StreamReader(openFileDialog1.FileName))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(List<fItem>));

                this.itemList = (List<fItem>) serializer.Deserialize(reader);
            }

            // .now we add each item to the listview1
            foreach (fItem t_item in this.itemList)
            {

                //. create the list view item
                ListViewItem listViewItem = new ListViewItem();

                listViewItem.ForeColor = t_item.TypeToColor();                       // text color
                listViewItem.Tag = t_item.id;                                        // id

                listViewItem.Text = t_item.TypeToString();                            // type                     
                listViewItem.SubItems.Add(Convert.ToString(t_item.Money) + "$");      // money
                listViewItem.SubItems.Add(t_item.Tag);                                // tag
                listViewItem.SubItems.Add(t_item.Date.ToString("dd/MM/yyyy"));        // date

                // .add the item to the list view
                listView1.Items.Add(listViewItem);
            }

            this.id = this.itemList[itemList.Count - 1].id;

            listView1.Refresh();
        }

        // user clicked the show the xml file button
        private void DisplayXML_Click(object sender, EventArgs e)
        {
            // .save the current items in temp.xml
            using (StreamWriter stream = new StreamWriter("temp.xml"))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(List<fItem>));

                serializer.Serialize(stream, this.itemList);
            }

            // .display the xml file
            try
            {
                // .might not work an all systems (if there is no program associated with xml files)
                System.Diagnostics.Process.Start("temp.xml");
            }
            catch
            {

            }
        }

        private void Total_Click(object sender, EventArgs e)
        {
            chartForm.SetTo0(); // we don`t need the data we calculated previously

            // .calculate how much was spent on each type
            foreach (fItem item in this.itemList)
            {
                switch (item.Type)
                {
                    case fItem.fType.Food:
                        chartForm.Food += item.Money;
                        break;

                    case fItem.fType.Drinks:
                        chartForm.Drinks += item.Money;
                        break;

                    case fItem.fType.Fun:
                        chartForm.Fun += item.Money;
                        break;

                    case fItem.fType.Gym:
                        chartForm.Gym += item.Money;
                        break;

                    case fItem.fType.Car:
                        chartForm.Car += item.Money;
                        break;

                    case fItem.fType.Bills:
                        chartForm.Bills += item.Money;
                        break;

                    case fItem.fType.Shopping:
                        chartForm.Shopping += item.Money;
                        break;

                    case fItem.fType.Clothing:
                        chartForm.Clothing += item.Money;
                        break;

                    case fItem.fType.Travel:
                        chartForm.Travel += item.Money;
                        break;

                    case fItem.fType.Medication:
                        chartForm.Medication += item.Money;
                        break;

                    case fItem.fType.Other:
                        chartForm.Other += item.Money;
                        break;
                }
            }

            // .fill the chart with the information
            chartForm.Reset();

            // .display the chart
            chartForm.ShowDialog();
        }

        // .the user cliced the clear button
        private void Clear_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to delete all ?", "Clear", MessageBoxButtons.OKCancel) ==
                DialogResult.OK)
            {
                // .remove all the items
                this.listView1.Items.Clear();
                this.itemList.Clear();
            }
        }
    }
}
