﻿namespace WindowsFormsApplication1
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listView1 = new System.Windows.Forms.ListView();
            this.Type = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Money = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Tag0 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Date = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Add = new System.Windows.Forms.Button();
            this.Delete = new System.Windows.Forms.Button();
            this.Edit = new System.Windows.Forms.Button();
            this.Total = new System.Windows.Forms.Button();
            this.LoadXML = new System.Windows.Forms.Button();
            this.SaveXML = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Clear = new System.Windows.Forms.Button();
            this.DisplayXML = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Type,
            this.Money,
            this.Tag0,
            this.Date});
            this.listView1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listView1.FullRowSelect = true;
            this.listView1.GridLines = true;
            this.listView1.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listView1.Location = new System.Drawing.Point(12, 12);
            this.listView1.MultiSelect = false;
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(614, 384);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // Type
            // 
            this.Type.Text = "Type";
            this.Type.Width = 109;
            // 
            // Money
            // 
            this.Money.Text = "Money";
            this.Money.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.Money.Width = 155;
            // 
            // Tag0
            // 
            this.Tag0.Text = "Tag";
            this.Tag0.Width = 145;
            // 
            // Date
            // 
            this.Date.Text = "Data";
            this.Date.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Date.Width = 194;
            // 
            // Add
            // 
            this.Add.Location = new System.Drawing.Point(23, 19);
            this.Add.Name = "Add";
            this.Add.Size = new System.Drawing.Size(100, 25);
            this.Add.TabIndex = 1;
            this.Add.Text = "Add";
            this.Add.UseVisualStyleBackColor = true;
            this.Add.Click += new System.EventHandler(this.Add_Click);
            // 
            // Delete
            // 
            this.Delete.Location = new System.Drawing.Point(23, 50);
            this.Delete.Name = "Delete";
            this.Delete.Size = new System.Drawing.Size(100, 25);
            this.Delete.TabIndex = 2;
            this.Delete.Text = "Delete";
            this.Delete.UseVisualStyleBackColor = true;
            this.Delete.Click += new System.EventHandler(this.Delete_Click);
            // 
            // Edit
            // 
            this.Edit.Location = new System.Drawing.Point(23, 81);
            this.Edit.Name = "Edit";
            this.Edit.Size = new System.Drawing.Size(100, 25);
            this.Edit.TabIndex = 4;
            this.Edit.Text = "Edit";
            this.Edit.UseVisualStyleBackColor = true;
            this.Edit.Click += new System.EventHandler(this.Edit_Click);
            // 
            // Total
            // 
            this.Total.Location = new System.Drawing.Point(23, 353);
            this.Total.Name = "Total";
            this.Total.Size = new System.Drawing.Size(100, 25);
            this.Total.TabIndex = 5;
            this.Total.Text = "Total";
            this.Total.UseVisualStyleBackColor = true;
            this.Total.Click += new System.EventHandler(this.Total_Click);
            // 
            // LoadXML
            // 
            this.LoadXML.Location = new System.Drawing.Point(23, 247);
            this.LoadXML.Name = "LoadXML";
            this.LoadXML.Size = new System.Drawing.Size(100, 25);
            this.LoadXML.TabIndex = 6;
            this.LoadXML.Text = "Load from XML";
            this.LoadXML.UseVisualStyleBackColor = true;
            this.LoadXML.Click += new System.EventHandler(this.LoadXML_Click);
            // 
            // SaveXML
            // 
            this.SaveXML.Location = new System.Drawing.Point(23, 216);
            this.SaveXML.Name = "SaveXML";
            this.SaveXML.Size = new System.Drawing.Size(100, 25);
            this.SaveXML.TabIndex = 7;
            this.SaveXML.Text = "Save to XML";
            this.SaveXML.UseVisualStyleBackColor = true;
            this.SaveXML.Click += new System.EventHandler(this.SaveXML_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Clear);
            this.groupBox1.Controls.Add(this.DisplayXML);
            this.groupBox1.Controls.Add(this.Add);
            this.groupBox1.Controls.Add(this.LoadXML);
            this.groupBox1.Controls.Add(this.SaveXML);
            this.groupBox1.Controls.Add(this.Delete);
            this.groupBox1.Controls.Add(this.Edit);
            this.groupBox1.Controls.Add(this.Total);
            this.groupBox1.Location = new System.Drawing.Point(632, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(160, 384);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            // 
            // Clear
            // 
            this.Clear.Location = new System.Drawing.Point(23, 112);
            this.Clear.Name = "Clear";
            this.Clear.Size = new System.Drawing.Size(100, 25);
            this.Clear.TabIndex = 9;
            this.Clear.Text = "Clear";
            this.Clear.UseVisualStyleBackColor = true;
            this.Clear.Click += new System.EventHandler(this.Clear_Click);
            // 
            // DisplayXML
            // 
            this.DisplayXML.Location = new System.Drawing.Point(23, 278);
            this.DisplayXML.Name = "DisplayXML";
            this.DisplayXML.Size = new System.Drawing.Size(100, 25);
            this.DisplayXML.TabIndex = 8;
            this.DisplayXML.Text = "Show XML file";
            this.DisplayXML.UseVisualStyleBackColor = true;
            this.DisplayXML.Click += new System.EventHandler(this.DisplayXML_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "Xml files (*.xml)|*.xml";
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Filter = "Xml files (*.xml)|*.xml";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(811, 415);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.listView1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "MainForm";
            this.Text = "Spender";
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.Button Add;
        private System.Windows.Forms.ColumnHeader Type;
        private System.Windows.Forms.ColumnHeader Money;
        private System.Windows.Forms.ColumnHeader Tag0;
        private System.Windows.Forms.ColumnHeader Date;
        private System.Windows.Forms.Button Delete;
        private System.Windows.Forms.Button Edit;
        private System.Windows.Forms.Button Total;
        private System.Windows.Forms.Button LoadXML;
        private System.Windows.Forms.Button SaveXML;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button DisplayXML;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button Clear;
    }
}

