﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class ChartForm : Form
    {
        public int Food;
        public int Drinks;
        public int Fun;
        public int Gym;
        public int Car;
        public int Bills;
        public int Shopping;
        public int Clothing;
        public int Travel;
        public int Medication;
        public int Other;
        public int Total;

        public ChartForm()
        {
            InitializeComponent();
        }

        public void SetTo0()
        {
            this.Food = 0;
            this.Drinks = 0;
            this.Fun = 0;
            this.Gym = 0;
            this.Car = 0;
            this.Bills = 0;
            this.Shopping = 0;
            this.Clothing = 0;
            this.Travel = 0;
            this.Medication = 0;
            this.Other = 0;
            this.Total = 0;
        }

        public void Reset()
        {
            // .fill up the chart
            chart1.Series["Series1"].Points[0].SetValueXY(1,Food);
            chart1.Series["Series1"].Points[0].Color = fItem.TypeToColor(fItem.fType.Food);

            
            chart1.Series["Series1"].Points[1].SetValueXY(2,Drinks);
            chart1.Series["Series1"].Points[1].Color = fItem.TypeToColor(fItem.fType.Drinks);

            chart1.Series["Series1"].Points[2].SetValueXY(3,Fun);
            chart1.Series["Series1"].Points[2].Color = fItem.TypeToColor(fItem.fType.Fun);

            chart1.Series["Series1"].Points[3].SetValueXY(4,Gym);
            chart1.Series["Series1"].Points[3].Color = fItem.TypeToColor(fItem.fType.Gym);

            chart1.Series["Series1"].Points[4].SetValueXY(5,Car);
            chart1.Series["Series1"].Points[4].Color = fItem.TypeToColor(fItem.fType.Car);

            chart1.Series["Series1"].Points[5].SetValueXY(6,Bills);
            chart1.Series["Series1"].Points[5].Color = fItem.TypeToColor(fItem.fType.Bills);

            chart1.Series["Series1"].Points[6].SetValueXY(7,Shopping);
            chart1.Series["Series1"].Points[6].Color = fItem.TypeToColor(fItem.fType.Shopping);

            chart1.Series["Series1"].Points[7].SetValueXY(8,Clothing);
            chart1.Series["Series1"].Points[7].Color = fItem.TypeToColor(fItem.fType.Clothing);

            chart1.Series["Series1"].Points[8].SetValueXY(9,Travel);
            chart1.Series["Series1"].Points[8].Color = fItem.TypeToColor(fItem.fType.Travel);

            chart1.Series["Series1"].Points[9].SetValueXY(10,Medication);
            chart1.Series["Series1"].Points[9].Color = fItem.TypeToColor(fItem.fType.Medication);

            chart1.Series["Series1"].Points[10].SetValueXY(11,Other);
            chart1.Series["Series1"].Points[10].Color = fItem.TypeToColor(fItem.fType.Other);

            this.Total = Food + Drinks + Fun + Gym + Car + Bills + Shopping +
                Clothing + Travel + Medication + Other;

            // .the amount of money the user spent
            textBox1.Text = Convert.ToString(this.Total + "$");

            chart1.ChartAreas[0].RecalculateAxesScale();
        }
    
    }
}
